const server = require('express')();
const next = require('next');

const { expressLogger, logger } = require('./lib/logger');
const nextConfig = require('./next.config');
const authServer = require('./server/authServer');
const uploadServer = require('./server/uploadServer');

const {
  serverRuntimeConfig: { PORT: port, IS_DEV: isDev },
} = nextConfig;

const app = next({ dev: isDev, dir: __dirname, conf: nextConfig });

const handle = app.getRequestHandler();

app.prepare().then(() => {
  server.use(expressLogger);
  authServer({ server, nextApp: app });
  uploadServer({ server });

  server.get('*', (req, res) => handle(req, res));
  // eslint-disable-next-line no-unused-vars
  server.use((err, req, res, _next) => {
    logger.error(err.stack);
    res.status(500).send('Something broke!');
  });
  server.listen(port, err => {
    if (err) throw err;
    logger.info(`> Ready on http://localhost:${port}`);
  });
});
