describe('Index page', () => {
  /*
   * Visits the page before each test
   */
  beforeEach(() => {
    cy.log(`Visiting /`);
    cy.visit('/');
  });

  it('header bar', () => {
    cy.get('.MuiAppBar-root').should('have.length', 1);
  });
  //   it('posts length', () => {
  //     cy.get('.site-footer .brand img').should('have.length', 1);
  //   });
});
