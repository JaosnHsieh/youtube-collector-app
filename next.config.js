require('dotenv').config();

const withCss = require('@zeit/next-css');

module.exports = withCss({
  target: 'server',
  // distDir: '.next',
  webpack: config => {
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: 'empty',
    };
    // remove alias due to conflicts with eslint alias setting
    // config.resolve.alias.components = path.join(__dirname, 'components');

    return config;
  },
  env: {
    GRAPHQL_API_URL: process.env.GRAPHQL_API_URL || 'https://xxxxxxxx/v1alpha1/graphql',
    IS_DEV: process.env.NODE_ENV !== 'production',
    FILE_SIZE_LIMIT: process.env.UPLOAD_FILE_SIZE_LIMIT || 10, // MB
    IS_DEV_BANNER_ON: process.env.IS_DEV_BANNER_ON || 'false',
    UPLOAD_STATIC_TAIL_PATH: process.env.UPLOAD_STATIC_TAIL_PATH,
  },
  serverRuntimeConfig: {
    // Will only be available on the server side
    HASURA_ADMIN_SECRET: process.env.HASURA_ADMIN_SECRET || 'adminsecret',
    JWT_SECRET:
      process.env.JWT_SECRET || '123jwtsecret321123jwtsecret321123jwtsecret321123jwtsecret321',
    COOKIE_PARSER_SECRET: process.env.COOKIE_PARSER_SECRET || 'cookieparsersecret',
    PORT: process.env.PORT || 3000,
    IS_DEV: process.env.NODE_ENV !== 'production',
    GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET,
    ROOT_URL: process.env.ROOT_URL || 'http://localhost:3000',
    UPLOAD_FILE_DIR_PATH: process.env.UPLOAD_FILE_DIR_PATH,
  },
});
