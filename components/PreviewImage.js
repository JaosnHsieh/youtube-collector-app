import React from 'react';
import PropTypes from 'prop-types';
import DeleteIcon from '@material-ui/icons/DeleteOutlineOutlined';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  wrapper: { height: '100%', position: 'relative', display: 'inline-block' },
  image: {
    width: '100%',
  },
  iconWrapper: {
    cursor: 'pointer',
    position: 'absolute',
    backgroundColor: 'rgba(0,0,0,0.8)',
    height: '24px', // equals to icon size
    width: '24px', // equals to icon size
    bottom: '0',
    right: '0',
  },
}));

const PreviewImage = ({ src = '', onDelete = () => {} }) => {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <img src={src} className={classes.image} alt="upload img" />
      <div className={classes.iconWrapper} onClick={onDelete}>
        <DeleteIcon htmlColor="white" />
      </div>
    </div>
  );
};

PreviewImage.propTypes = {
  src: PropTypes.string.isRequired,
  onDelete: PropTypes.func,
};

export default PreviewImage;
