import React from 'react';
import PropTypes from 'prop-types';
// import Debug from 'debug';
import { makeStyles } from '@material-ui/core/styles';
import _get from 'lodash/get';
import dayjs from 'dayjs';
// import 'dayjs/locale/zh-tw'; // load on demand
import Link from 'next/link';
import relativeTime from 'dayjs/plugin/relativeTime';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
// import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import SingleLineGridList from './SingleLineGridList';

// const debug = Debug('YCA:components:PostCard');
dayjs.extend(relativeTime);

const useStyles = makeStyles(theme => ({
  card: {
    width: '100%',
    marginBottom: '10px',
    padding: '0 5px',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
  content: {
    cursor: 'pointer',
  },
}));

function PostCard({ post = {}, onClickFavoriteIcon }) {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={
          _get(post, 'owner.files[0].url', '').length > 0 ? (
            <Avatar
              aria-label={_get(post, 'owner.display_name')}
              src={_get(post, 'owner.files[0].url')}
              className={classes.avatar}
            >
              {}
            </Avatar>
          ) : (
            <Avatar aria-label={_get(post, 'owner.display_name')} className={classes.avatar}>
              {_get(post, 'owner.display_name[0]')}
            </Avatar>
          )
        }
        action={
          <IconButton aria-label="Settings">
            <MoreVertIcon />
          </IconButton>
        }
        title={_get(post, 'title', '')}
        subheader={dayjs(_get(post, 'created_at')).fromNow()}
      />
      <SingleLineGridList
        imgUrls={_get(post, 'files', []).map(file => {
          const imageSrc =
            _get(file, 'url') ||
            `${process.env.UPLOAD_STATIC_TAIL_PATH}/${_get(file, 'saved_name')}`;
          return imageSrc;
        })}
      />
      <CardContent className={classes.content}>
        <Link href={`/post?id=${_get(post, 'id')}`}>
          <Typography variant="body2" color="textSecondary" component="p">
            {_get(post, 'content', '')}
          </Typography>
        </Link>
      </CardContent>

      <CardActions disableSpacing>
        <IconButton aria-label="Add to favorites" onClick={onClickFavoriteIcon}>
          <FavoriteIcon color={_get(post, 'myLikes', []).length > 0 ? 'secondary' : 'disabled'} />
        </IconButton>
        <IconButton aria-label="Share">
          <ShareIcon />
        </IconButton>
      </CardActions>
    </Card>
  );
}

PostCard.propTypes = {
  post: PropTypes.object.isRequired,
  onClickFavoriteIcon: PropTypes.func,
};
export default PostCard;
