// from https://github.com/mui-org/material-ui/tree/master/examples/nextjs

import { createMuiTheme } from '@material-ui/core/styles';

// theme picked from: https://material.io/tools/color/#!/?view.left=0&view.right=1&primary.color=f1f8e9&secondary.color=3F51B5

// Create a theme instance.
const theme = createMuiTheme({
  // https://material-ui.com/customization/spacing/
  spacing: 8,
  palette: {
    primary: {
      light: '#ffffff',
      main: '#f1f8e9',
      dark: '#bec5b7',
      contrastText: '#000000',
    },
    secondary: {
      light: '#ff7543',
      main: '#dd2c00',
      dark: '#9f0000',
      contrastText: '#000000',
    },
  },
});

export default theme;
