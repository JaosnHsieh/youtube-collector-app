import InfiniteScroll from 'react-infinite-scroller';
import PropTypes from 'prop-types';
// import Debug from 'debug';
// import _get from 'lodash/get';
import { makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import PostCard from './PostCard';

// const debug = Debug('YCA:components:ScrollPosts');

export const useStyles = makeStyles(theme => ({
  main: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    [theme.breakpoints.up('sm')]: {
      marginTop: theme.spacing(1),
    },
  },
  loader: { width: '100%', position: 'fixed', top: 0 },
  infiniteScroll: { width: '100%', maxWidth: 600 },
}));

const ScrollPosts = ({
  posts = [],
  onLoadMore,
  isLoading = false,
  hasMore = true,
  onClickFavoriteIcon = () => {},
}) => {
  const classes = useStyles();
  return (
    <main className={classes.main}>
      {isLoading && (
        <div className={classes.loader}>
          <LinearProgress color="secondary" />
        </div>
      )}

      <InfiniteScroll
        className={classes.infiniteScroll}
        pageStart={0}
        loadMore={() => {
          onLoadMore();
        }}
        hasMore={hasMore}
      >
        {posts.map((post, i) => (
          <PostCard
            // eslint-disable-next-line react/no-array-index-key
            key={i}
            post={post}
            onClickFavoriteIcon={() => {
              onClickFavoriteIcon({ post });
            }}
          />
        ))}
      </InfiniteScroll>
    </main>
  );
};

ScrollPosts.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object).isRequired,
  onLoadMore: PropTypes.func,
  isLoading: PropTypes.bool.isRequired,
  hasMore: PropTypes.bool,
  onClickFavoriteIcon: PropTypes.func,
};

export default ScrollPosts;
