/**
 * there is no React version FileInput in Uppy but similar Component <DragDrop>
 * below code refer to
 * https://github.com/transloadit/uppy/blob/master/packages/%40uppy/react/src/DragDrop.js
 * https://github.com/transloadit/uppy/issues/813#issuecomment-494348650
 * https://stackoverflow.com/a/27564324/6414615
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import FileInput from '@uppy/file-input';
import _omit from 'lodash/omit';

// comment due to custimization style
// import '@uppy/file-input/dist/style.min.css';

// const h = React.createElement;

/**
 * React component that renders an area in which files can be dropped to be
 * uploaded.
 */

class UppyInput extends React.Component {
  componentDidMount() {
    this.installPlugin();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.uppy !== this.props.uppy) {
      this.uninstallPlugin(prevProps);
      this.installPlugin();
    }
  }

  componentWillUnmount() {
    this.uninstallPlugin();
  }

  installPlugin() {
    const { uppy } = this.props;
    const options = Object.assign({ id: 'react:FileInput' }, this.props, {
      target: this.container,
      ..._omit(this.props, ['component', 'uppy']),
    });
    delete options.uppy;

    uppy.use(FileInput, options);

    this.plugin = uppy.getPlugin(options.id);
  }

  uninstallPlugin(props = this.props) {
    const { uppy } = props;

    uppy.removePlugin(this.plugin);
  }

  render() {
    return (
      <div style={{ position: 'relative', display: 'inline-block' }}>
        <style>
          {`
            div.uppy-FileInput-container{
                width: 100%;
                height: 100%;
                position: absolute;
                top: 0;
                left: 0;
                opacity: 0;
             }
             button.uppy-FileInput-btn{
                width: 100%;
                height: 100%;
                cursor: pointer;
            }
          `}
        </style>
        <div
          ref={ref => {
            if (ref) {
              this.container = ref;
            }
          }}
        />
        {this.props.component}
      </div>
    );
  }
}

UppyInput.propTypes = {
  component: PropTypes.element.isRequired,
  uppy: PropTypes.object.isRequired,
};

export default UppyInput;
