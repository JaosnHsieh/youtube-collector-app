// https://material-ui.com/components/grid-list/#single-line-grid-list
import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    flexWrap: 'nowrap',
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: 'translateZ(0)',
  },
  // gridListOneChild is for fixing when there is only one image, it looks too small
  gridListOneChild: {
    '& li': {
      width: '100%!important',
    },
  },
}));

function SingleLineGridList({ imgUrls = [] }) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <GridList
        className={`${classes.gridList} ${imgUrls.length === 1 ? classes.gridListOneChild : ''}`}
        cols={2.5}
      >
        {imgUrls.map((url, i) => (
          // eslint-disable-next-line react/no-array-index-key
          <GridListTile key={i}>
            <img src={url} alt={url} />
          </GridListTile>
        ))}
      </GridList>
    </div>
  );
}

SingleLineGridList.propTypes = {
  imgUrls: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default SingleLineGridList;
