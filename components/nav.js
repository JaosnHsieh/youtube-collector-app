import React from 'react';
import Link from 'next/link';

const Nav = () => (
  <nav>
    <ul>
      <li>
        <Link href="/profile">Profile</Link>
      </li>
      <li>
        <Link href="/login">Login</Link>
      </li>
      <li>
        <a href="/logout">Logout</a>
      </li>
      <li>
        <Link prefetch href="/">
          <span>Home</span>
        </Link>
      </li>
      <li>
        <Link prefetch href="/signup">
          <span>signup</span>
        </Link>
      </li>
      <li>
        <Link prefetch href="/users">
          <span>Users</span>
        </Link>
      </li>
    </ul>

    <style jsx>{`
      :global(body) {
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, Avenir Next, Avenir, Helvetica, sans-serif;
      }
      nav {
        text-align: center;
      }
      ul {
        display: flex;
        justify-content: space-between;
      }
      nav > ul {
        padding: 4px 16px;
      }
      li {
        display: flex;
        padding: 6px 8px;
      }
      a {
        color: #067df7;
        text-decoration: none;
        font-size: 13px;
      }
    `}</style>
  </nav>
);

export default Nav;
