import React from 'react';
import PropTypes from 'prop-types';
import Uppy from '@uppy/core';
import ThumbnailGenerator from '@uppy/thumbnail-generator';
import xhr from '@uppy/xhr-upload';
import _get from 'lodash/get';
import { StatusBar } from '@uppy/react';
import Debug from 'debug';
import '@uppy/core/dist/style.min.css';
import '@uppy/status-bar/dist/style.min.css';
import PreviewImage from './PreviewImage';
import UppyInput from './UppyInput';

const debug = Debug('YCA:UppyUpload:');
const maxFileSize = process.env.MAX_FILE_SIZE || 10;
const maxNumberOfFiles = process.env.MAX_NUMBER_OF_FILES || 5;
const bitsToMb = 1000000;
// const isDev = process.env.IS_DEV;

class UppyUpload extends React.Component {
  constructor(props) {
    super(props);
    debug('maxNumberOfFiles', maxNumberOfFiles);
    debug('maxFileSize', maxFileSize);
    const uppy = Uppy({
      meta: { type: 'avatar' },
      restrictions: {
        maxNumberOfFiles,
        maxFileSize: maxFileSize * bitsToMb /** 10 mb  https://uppy.io/blog/2017/07/0.17/ */,
        allowedFileTypes: ['image/*'],
      },
      onBeforeFileAdded: currentFile => {
        debug('onBeforeFileAdded');
        const fileSize = _get(currentFile, 'data.size', 0);
        debug('fileSize', fileSize);
        debug('fileSize > maxFileSize * bitsToMb', fileSize > maxFileSize * bitsToMb);
        if (fileSize > maxFileSize * bitsToMb) {
          // eslint-disable-next-line no-alert
          alert(`File size limit is ${maxFileSize} MB`);
        }
      },
      autoProceed: true,
      debug: true, // isDev,
    });
    uppy.use(ThumbnailGenerator, {
      thumbnailWidth: 100,
      thumbnailHeight: 100,
    });

    uppy.use(xhr, { endpoint: '/upload' });

    uppy.on('complete', result => {
      this.props.onUploadComplete(result);
    });

    uppy.on('thumbnail:generated', (file, preview) => {
      this.setState({ thumbnails: [...this.state.thumbnails, { file, preview }] });
    });

    uppy.on('upload-error', (file, error, response) => {
      debug('upload-error file.id:', file.id);
      debug('upload-error error', error);
      debug('upload-error response', response);
    });

    uppy.on('restriction-failed', (file, error) => {
      debug('restriction-failed file', file);
      debug('restriction-failed error', error);
      // eslint-disable-next-line no-alert
      alert(error);
    });

    this.uppy = uppy;
  }
  state = {
    thumbnails: [],
  };

  componentWillUnmount() {
    this.uppy.close();
  }

  render() {
    const renderStatusBar = <StatusBar uppy={this.uppy} hideAfterFinish />;
    const renderUppyInput = <UppyInput uppy={this.uppy} component={this.props.component} />;
    const renderThumbnailsBar = (
      <>
        {this.state.thumbnails.length > 0 && (
          <div>
            {this.state.thumbnails.map(({ file, preview }) => (
              <PreviewImage
                key={preview}
                onDelete={() => {
                  debug('PreviewImage onDelete file', file);
                  const uppyFileId = file.id;
                  this.uppy.removeFile(uppyFileId);
                  this.setState(prevState => ({
                    thumbnails: prevState.thumbnails.filter(t => t.file.id !== uppyFileId),
                  }));
                  if (this.props.onFileDelete) {
                    this.props.onFileDelete(uppyFileId);
                  }
                }}
                src={preview}
              />
            ))}
          </div>
        )}
      </>
    );
    return this.props.children({
      renderStatusBar,
      renderUppyInput,
      renderThumbnailsBar,
      clearAllThumbnails: () => {
        this.setState({ thumbnails: [] });
      },
    });
  }
}

UppyUpload.propTypes = {
  component: PropTypes.element.isRequired,
  onUploadComplete: PropTypes.func,
  onFileDelete: PropTypes.func,
  children: PropTypes.func.isRequired,
};

export default UppyUpload;
