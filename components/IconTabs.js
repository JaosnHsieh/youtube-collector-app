import React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
// eslint-disable-next-line spaced-comment
//https://material.io/tools/icons
import HomeIcon from '@material-ui/icons/HomeOutlined';
import NotificationIcon from '@material-ui/icons/NotificationImportantOutlined';
import SubscriptionIcon from '@material-ui/icons/SubscriptionsOutlined';
import Link from 'next/link';
import { withRouter } from 'next/router';

const LinkTab = ({ href, ...props }) => (
  <Link href={href}>
    <Tab {...props} />
  </Link>
);
LinkTab.propTypes = {
  href: PropTypes.string.isRequired,
};

const ROUTE_PATHS = ['/', '/subscriptions', '/notifications'];

function IconLabelTabs({ router, isLogined = false }) {
  const activePathIndex =
    ROUTE_PATHS.indexOf(router.pathname) === -1 ? false : ROUTE_PATHS.indexOf(router.pathname);
  return (
    <Tabs
      value={activePathIndex}
      // onChange={handleChange}
      variant="fullWidth"
      indicatorColor="secondary"
      textColor="secondary"
    >
      <LinkTab href={ROUTE_PATHS[0]} icon={<HomeIcon />} label="Home" />
      {isLogined && (
        <LinkTab href={ROUTE_PATHS[1]} icon={<SubscriptionIcon />} label="Subscriptions" />
      )}
      {isLogined && (
        <LinkTab href={ROUTE_PATHS[2]} icon={<NotificationIcon />} label="Notifications" />
      )}
    </Tabs>
  );
}

IconLabelTabs.propTypes = {
  router: PropTypes.object.isRequired,
  isLogined: PropTypes.bool,
};
export default withRouter(IconLabelTabs);
