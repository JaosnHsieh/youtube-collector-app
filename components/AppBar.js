import React from 'react';
import PropTypes from 'prop-types';
import _get from 'lodash/get';
import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import Hidden from '@material-ui/core/Hidden';
import Paper from '@material-ui/core/Paper';
import Avatar from '@material-ui/core/Avatar';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Link from 'next/link';
import IconTabs from './IconTabs';

export const getAppBarPropsFromUser = user => {
  const isLogined = !!user;
  const avatarUrl = _get(user, 'files[0].url', '');
  const displayName = _get(user, 'displayName', '') || _get(user, 'name', '');
  return {
    isLogined,
    avatarUrl,
    displayName,
  };
};
const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  titleAnchor: {
    textDecoration: 'none',
    color: 'inherit',
    outline: 0,
    cursor: 'pointer',
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  tabs: {
    flexGrow: 1,
    backgroundColor: theme.palette.primary.main,
  },
  imgIconButton: {
    maxWidth: '35%',
  },
  signInImg: {
    maxWidth: '100%',
  },
}));

function SearchAppBar({
  isLogined,
  avatarUrl,
  displayName,
  defaultKeyword = '',
  onSearch = () => {},
  isInitFocusSearchInput = false,
}) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const isMenuOpen = Boolean(anchorEl);

  function handleProfileMenuOpen(event) {
    setAnchorEl(event.currentTarget);
  }
  function handleMenuClose() {
    setAnchorEl(null);
  }

  const menuId = 'menu-id';
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <a href="/logout" alt="logout">
        <MenuItem onClick={handleMenuClose}>Logout</MenuItem>
      </a>
    </Menu>
  );

  const renderImageAvatar = isLogined ? (
    <Avatar alt={displayName} src={avatarUrl}>
      {!avatarUrl && displayName[0]}
    </Avatar>
  ) : (
    <a href="/auth/google">
      <img
        className={classes.signInImg}
        alt="google-sign-in"
        src="/static/images/google-icons/btn_google_signin_light_normal_web.png"
      />
    </a>
  );

  return (
    <>
      <div className={classes.grow}>
        <AppBar position="static">
          <Toolbar>
            {/* <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="Open drawer"
            >
              <MenuIcon />
            </IconButton> */}
            <Typography className={classes.title} variant="h6" noWrap>
              <Link href="/">
                <a className={classes.titleAnchor} href="/">
                  Share My Tube
                </a>
              </Link>
            </Typography>
            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <InputBase
                autoFocus={isInitFocusSearchInput}
                placeholder="Search…"
                defaultValue={defaultKeyword}
                onChange={e => {
                  onSearch(e.target.value);
                }}
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
                inputProps={{ 'aria-label': 'Search' }}
              />
            </div>
            <div>
              <Hidden implementation="css" smDown>
                <IconTabs isLogined={isLogined} />
              </Hidden>
            </div>
            <div className={classes.grow} />
            <div className={classes.sectionDesktop} />
            <IconButton
              aria-label="current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={isLogined ? handleProfileMenuOpen : () => {}}
              color="inherit"
              className={classes.imgIconButton}
            >
              {renderImageAvatar}
            </IconButton>
          </Toolbar>
        </AppBar>
      </div>
      {renderMenu}
      <Hidden implementation="css" mdUp>
        <Paper className={classes.tabs}>
          <IconTabs isLogined={isLogined} />
        </Paper>
      </Hidden>
    </>
  );
}

SearchAppBar.propTypes = {
  isLogined: PropTypes.bool.isRequired,
  avatarUrl: PropTypes.string,
  displayName: PropTypes.string,
  defaultKeyword: PropTypes.string,
  onSearch: PropTypes.func,
  isInitFocusSearchInput: PropTypes.bool,
};
export default SearchAppBar;
