import React, { useState } from 'react';
import _get from 'lodash/get';
import produce from 'immer';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Avatar from '@material-ui/core/Avatar';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import AddIcon from '@material-ui/icons/Add';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import Debug from 'debug';
import UppyUpload from './UppyUpload';

const debug = Debug('YCA:components:CreatePostFormCard:');
export const getCardPropsFromUser = user => {
  const avatarUrl = _get(user, 'files[0].url', '');
  const displayName = _get(user, 'displayName', '') || _get(user, 'name', '');
  return {
    avatarUrl,
    displayName,
  };
};

const useStyles = makeStyles(({ spacing }) => ({
  wrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: spacing(0.5),
    marginBottom: spacing(0.5),
  },
  card: {
    width: '100%',
    maxWidth: 600,
    padding: spacing(0.5),
    display: 'flex',
    alignItems: 'start',
  },
  inputBox: {
    width: '80%',
  },
  input: {
    width: '100%',
  },
  iconButton: {
    padding: 10,
    flex: 1,
  },
  divider: {
    width: 1,
    height: 28,
    margin: 4,
  },
  toolBarButton: {
    margin: spacing(1),
  },
  toolBarInput: {
    display: 'none',
  },
}));

function CreatePostFormCard({ avatarUrl, displayName, onSubmit = () => {} }) {
  const classes = useStyles();
  const defaultEditingPost = {
    content: '',
    fileIds: [],
    uppyFileIdToFileIdMap: {},
  };
  const [editingPost, setEditingPost] = useState(defaultEditingPost);
  const renderToolBarGridContainer = (
    <>
      <UppyUpload
        onFileDelete={uppyFileId => {
          debug('onFileDelete uppyFileId', uppyFileId);
          setEditingPost({
            ...editingPost,
            fileIds: editingPost.fileIds.filter(
              fid => fid !== editingPost.uppyFileIdToFileIdMap[uppyFileId],
            ),
          });
        }}
        onUploadComplete={result => {
          debug('onUploadComplete reuslt', result);
          _get(result, 'successful', []).map(s => {
            const fileId = _get(s, 'response.body.fileId', null);
            const uppyFileId = _get(s, 'id', null);
            return setEditingPost(
              produce(draft => {
                draft.fileIds.push(fileId);
                draft.uppyFileIdToFileIdMap[uppyFileId] = fileId;
              }),
            );
          });
        }}
        component={<PhotoCamera color="secondary" />}
      >
        {({ renderStatusBar, renderUppyInput, renderThumbnailsBar, clearAllThumbnails }) => (
          <>
            <Grid container spacing={1}>
              <Grid item xs={12}>
                {renderThumbnailsBar}
              </Grid>
              <Grid item xs={12}>
                {renderStatusBar}
              </Grid>
              <Grid item xs={12}>
                <Grid container spacing={0} justify="flex-end">
                  <Grid item xs={2} md={2}>
                    <IconButton color="secondary" aria-label="create post" size="small">
                      {renderUppyInput}
                    </IconButton>
                  </Grid>
                  <Grid item xs={2} md={2}>
                    <IconButton
                      disabled={!editingPost.content.length > 0 && !editingPost.fileIds.length > 0}
                      onClick={async () => {
                        await onSubmit(editingPost);
                        clearAllThumbnails();
                        setEditingPost({ ...defaultEditingPost });
                      }}
                      color="secondary"
                      aria-label="create post"
                      size="small"
                    >
                      <AddIcon />
                    </IconButton>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </>
        )}
      </UppyUpload>
    </>
  );
  const renderCard = (
    <div className={classes.wrapper}>
      <Paper className={classes.card}>
        <IconButton
          aria-label="current user"
          //   aria-controls={menuId}
          aria-haspopup="true"
          //   onClick={isLogined ? handleProfileMenuOpen : () => {}}
          color="inherit"
          className={classes.iconButton}
        >
          <Avatar alt={displayName} src={avatarUrl}>
            {!avatarUrl && displayName[0]}
          </Avatar>
        </IconButton>

        <Box className={classes.inputBox}>
          <Grid container>
            <Grid item xs={12}>
              <TextField
                value={editingPost.content}
                onChange={e => {
                  const { target: { value = '' } = {} } = e;
                  setEditingPost(post => ({ ...post, content: value }));
                }}
                className={classes.input}
                label="Share youtube video....."
                rowsMax={5}
                variant="outlined"
                id="custom-css-outlined-input"
                multiline
              />
            </Grid>
          </Grid>
          {renderToolBarGridContainer}
        </Box>
      </Paper>
    </div>
  );
  return renderCard;
}

CreatePostFormCard.propTypes = {
  avatarUrl: PropTypes.string,
  displayName: PropTypes.string,
  onSubmit: PropTypes.func.isRequired,
};

export default CreatePostFormCard;
