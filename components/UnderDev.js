import React from 'react';

const UnderDev = () =>
  process.env.IS_DEV_BANNER_ON === 'true' ? (
    <>
      <style>
        {`
        .typewriter {
            font-family: Courier, monospace;
          position:fixed;
          bottom:0;
          background-color:black;
        }
        
        .typewriter-text {
          color:#00ff00;
            display: inline-block;
              overflow: hidden;
              letter-spacing: 2px;
             animation: typing 5s steps(30, end), blink .75s step-end infinite;
            white-space: nowrap;
            font-size: 13px;
            font-weight: 700;
            border-right: 4px solid orange;
            box-sizing: border-box;
        }
        
        @keyframes typing {
            from { 
                width: 0% 
            }
            to { 
                width: 100% 
            }
        }
        
        @keyframes blink {
            from, to { 
                border-color: transparent 
            }
            50% { 
                border-color: orange; 
            }
        }
        
      `}
      </style>
      <div className="typewriter">
        <div className="typewriter-text">dev mode, data might be cleared.</div>
      </div>
    </>
  ) : null;

export default UnderDev;
