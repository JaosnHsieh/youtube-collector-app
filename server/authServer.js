const _ = require('lodash');
const bodyParser = require('body-parser');
const cookieSession = require('cookie-session');
const cookieParser = require('cookie-parser');
const jwt = require('jsonwebtoken');
const { promisify } = require('util');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const nextConfig = require('../next.config');
const { graphqlClient } = require('./graphql');
const debug = require('debug')('YCA:authServer.js');

const jwtSign = promisify(jwt.sign);

const {
  serverRuntimeConfig: {
    JWT_SECRET,
    COOKIE_PARSER_SECRET,
    GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET,
    ROOT_URL,
  },
} = nextConfig;

const insertDefaultChannel = async ({ userId, channelName, isDefault = true }) => {
  try {
    const response = await graphqlClient.request(
      /* GraphQL */ `
        mutation($name: String!, $ownerId: Int!, $isDefault: Boolean!) {
          insert_channel(objects: { name: $name, owner_id: $ownerId, is_default: $isDefault }) {
            returning {
              id
            }
          }
        }
      `,
      {
        ownerId: userId,
        name: channelName,
        isDefault,
      },
    );
    const createdChannel = _.get(response, 'insert_channel.returning[0]');
    debug(`createdChannel`, createdChannel);
    if (!createdChannel) {
      throw new Error('insertDefaultChannel return error');
    }
    return createdChannel;
  } catch (err) {
    debug('insertDefaultChannel err', err);
    throw err;
  }
};

const upsertGoogleAvatarFileToUser = async ({
  userId = '',
  googleId = '',
  avatarUrl = '',
  isOldUser = false,
}) => {
  try {
    const INSERT_FILE = /* GraphQL */ `
      insert_file(objects: { user_id: $userId, name: $name, url: $avatarUrl }) {
        affected_rows
      }`;
    const UPDATE_FILE = /* GraphQL */ `
      update_file(_set: {url: $avatarUrl}, where: { id: { _eq: $userId} }) {
        affected_rows
        returning{
          id
          url
        }
      }`;
    const response = await graphqlClient.request(
      /* GraphQL */ `
        mutation($userId: Int!, $name: String!, $avatarUrl: String!) {
          ${isOldUser ? UPDATE_FILE : INSERT_FILE}
        }
      `,
      {
        name: `${googleId}-avatar`,
        avatarUrl,
        userId,
      },
    );
    const affectedRows = _.get(
      response,
      `${isOldUser ? 'insert_file' : 'update_file'}.affected_rows`,
      0,
    );
    if (!affectedRows === 0) {
      throw new Error('upsertGoogleAvatarFileToUser return error');
    }
  } catch (err) {
    throw err;
  }
};
const USER_FIELDS = /* GraphQL */ `
      id
      google_id
      name
      email
      display_name
      role
`;
const insertUser = async ({
  name = '',
  email = '',
  password = null,
  displayName = '',
  googleId = null,
  role = 'User',
}) => {
  try {
    const response = await graphqlClient.request(
      /* GraphQL */ `mutation ($name: String!, $password: String, $email: String!, $displayName: String!, $googleId: String, $role: String!) {
        insert_user(objects: {name: $name, password: $password, email: $email, display_name: $displayName, role: $role, google_id: $googleId}) {
          returning {
            ${USER_FIELDS}
          }
        }
      }`,
      {
        name,
        email,
        displayName,
        googleId,
        role,
        password,
      },
    );
    const createdUser = _.get(response, 'insert_user.returning[0]');
    await insertDefaultChannel({
      userId: _.get(createdUser, 'id'),
      channelName: 'default channel name',
    });
    if (!createdUser) {
      throw new Error('insertUser return error');
    }
    return createdUser;
  } catch (err) {
    throw err;
  }
};

const queryUserByGoogleId = async ({ googleId = '' }) => {
  if (!googleId) {
    throw new Error('queryUserByGoogleId: googleId is a must');
  }
  const { user: [user] = [] } = await graphqlClient.request(
    `query($email:String, $googleId:String){
      user(where:{google_id:{_eq:$googleId}}){
        ${USER_FIELDS}
      }
    }`,
    {
      googleId,
    },
  );
  return user;
};

const queryUserByEmailAndPwd = async ({ email = '', password = '' }) => {
  if (!email || !password) {
    throw new Error('queryUserByEmailAndPwd: email or password is a must.');
  }
  const { user: [user] = [] } = await graphqlClient.request(
    `query($email:String!, $password:String!){
      user(where:{_and:[{email:{ _eq: $email }},{password:{_eq:$password}}]}){
        ${USER_FIELDS}
      }
    }`,
    {
      email,
      password,
    },
  );
  return user;
};

const googleSignInOrSignUp = async ({
  name = '',
  email = '',
  displayName = '',
  role = 'user',
  googleId = '',
}) => {
  const user = await queryUserByGoogleId({ googleId });
  if (user) {
    return {
      user,
      isOldUser: true,
    };
  }
  const newUser = await insertUser({ name, email, displayName, role, googleId });
  return {
    user: newUser,
    isOldUser: false,
  };
};

const setJwtInSessionAndCookie = async ({ req, res, userId }) => {
  const newUserJwt = await jwtSign(
    {
      'https://hasura.io/jwt/claims': {
        'X-Hasura-Allowed-Roles': ['user'],
        'X-Hasura-Default-Role': 'user',
        'X-Hasura-User-Id': `${userId}`,
      },
    },
    JWT_SECRET,
    {
      /**
       * 1 year
       * unit: second
       * https://www.npmjs.com/package/jsonwebtoken
       *
       * */

      expiresIn: 60 * 60 * 24 * 365,
    },
  );
  req.session.userId = userId;
  req.session.jwt = newUserJwt;
  res.cookie('jwt', newUserJwt, {
    signed: false,
    /**
     * 1 year
     * unit: millisecond
     *
     */
    maxAge: 1000 * 60 * 60 * 24 * 356,
    httpOnly: false,
  });
};

/**
 * @param {Object}  options - A string param.
 * @param {import("express").Application} options.server
 * @param {import("next").Server} options.nextApp
 */
module.exports = ({ server, nextApp }) => {
  server.use(cookieParser(COOKIE_PARSER_SECRET));
  server.use(bodyParser.json()); // to support JSON-encoded bodies
  server.use(
    bodyParser.urlencoded({
      // to support URL-encoded bodies
      extended: true,
    }),
  );

  server.use(
    cookieSession({
      name: 'session',
      keys: ['cookie-session-keys'],
      // Cookie Options
      maxAge: 24 * 60 * 60 * 1000, // 24 hours
    }),
  );
  server.use(passport.initialize());

  const verify = async (accessToken, refreshToken, profile, verified) => {
    let name;
    let email;
    let googleId;
    let avatarUrl;
    if (profile.photos && profile.photos.length > 0) {
      avatarUrl = profile.photos[0].value.replace('sz=50', 'sz=128');
    }

    if (profile.id) {
      googleId = profile.id;
    }

    if (profile.displayName) {
      name = profile.displayName;
    }
    if (profile.emails) {
      email = profile.emails[0].value;
    }

    try {
      const { user, isOldUser } = await googleSignInOrSignUp({
        name,
        email,
        displayName: name,
        googleId,
      });
      if (avatarUrl) {
        await upsertGoogleAvatarFileToUser({
          userId: user && user.id,
          googleId,
          avatarUrl,
          isOldUser,
        });
      }
      verified(null, user);
    } catch (err) {
      verified(err);
    }
  };
  passport.use(
    new GoogleStrategy(
      {
        clientID: GOOGLE_CLIENT_ID,
        clientSecret: GOOGLE_CLIENT_SECRET,
        callbackURL: `${ROOT_URL}/oauth2callback`,
        userProfileURL: 'https://www.googleapis.com/oauth2/v3/userinfo',
      },
      verify,
    ),
  );

  server.get(
    '/auth/google',
    passport.authenticate('google', {
      session: false,
      scope: ['profile', 'email'],
      prompt: 'select_account',
    }),
  );

  server.get(
    '/oauth2callback',
    passport.authenticate('google', {
      failureRedirect: '/login',
      session: false,
    }),
    async (req, res) => {
      await setJwtInSessionAndCookie({ req, res, userId: req.user.id });
      res.redirect('/');
    },
  );

  passport.use(
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'passwd',
      },

      async (email, password, done) => {
        const user = await queryUserByEmailAndPwd({ email, password });
        if (user) {
          return done(null, user);
        }
        return done(new Error('Cannot find user'));
      },
    ),
  );

  server.get('/login', (req, res) => {
    if (!req.session.userId) {
      return nextApp.render(req, res, '/login');
    }
    return res.redirect('/');
  });

  server.post(
    '/login',
    passport.authenticate('local', { failureRedirect: '/login', session: false }),
    async (req, res) => {
      await setJwtInSessionAndCookie({ req, res, userId: req.user.id });
      return res.redirect('/');
    },
  );

  server.get('/logout', (req, res) => {
    req.session = null;
    res.clearCookie('jwt');
    return res.redirect('/');
  });

  server.get('/signup', (req, res) => {
    if (!req.session.userId) {
      return nextApp.render(req, res, '/signup');
    }
    return res.redirect('/');
  });

  server.post('/signup', async (req, res, next) => {
    try {
      if (req.session.userId) {
        return res.redirect('/');
      }
      const { email = '', password = '', username = '' } = req.body;
      const user = await insertUser({ email, displayName: username, name: username, password });
      await setJwtInSessionAndCookie({ req, res, userId: user.id });
      return res.redirect('/');
    } catch (err) {
      return next(err);
    }
  });
};
