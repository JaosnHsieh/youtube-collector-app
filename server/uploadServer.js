/* eslint-disable camelcase */
const formidable = require('formidable');
const fs = require('fs');
const { promisify } = require('util');
const path = require('path');
const _get = require('lodash/get');
const { graphqlClient } = require('./graphql');
const nextConfig = require('../next.config');
const debug = require('debug')('YCA:uploadServer');

const fsExistsPromise = promisify(fs.exists);
const fsMkdirPromise = promisify(fs.mkdir);

const createDirIfNotExists = async dirPath => {
  try {
    if (!(await fsExistsPromise(dirPath))) {
      await fsMkdirPromise(dirPath);
    }
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
  }
};

const {
  env: { FILE_SIZE_LIMIT = 10 },
  serverRuntimeConfig: { UPLOAD_FILE_DIR_PATH },
} = nextConfig;
createDirIfNotExists(UPLOAD_FILE_DIR_PATH);
/**
 * refer to https://github.com/transloadit/uppy/tree/master/examples/node-xhr
 *  */

/**
 * @param {import("express").Application} options.server
 * @param {Object}  options
 */
module.exports = ({
  server,
  options: {
    uploadApiEndpoint = '/upload',
    uploadDir = UPLOAD_FILE_DIR_PATH || path.join(__dirname, '..', 'static', 'images', 'uploads'),
  } = {},
}) => {
  debug('uploadDir', uploadDir);
  // eslint-disable-next-line no-shadow
  const insertFile = async ({ saved_name, name, size, path, url, user_id, post_id }) => {
    try {
      const INSERT_FILE = /* GraphQL */ `
        mutation(
          $saved_name: String!
          $name: String!
          $size: Int!
          $path: String!
          $url: String
          $user_id: Int!
          $post_id: Int
        ) {
          insert_file(
            objects: {
              saved_name: $saved_name
              name: $name
              path: $path
              user_id: $user_id
              post_id: $post_id
            }
          ) {
            returning {
              id
            }
          }
        }
      `;
      debug(`insertFile before insert data`, {
        saved_name,
        name,
        size,
        path,
        url,
        user_id,
        post_id,
      });
      const res = await graphqlClient.request(INSERT_FILE, {
        saved_name,
        name,
        size,
        path,
        url,
        user_id,
        post_id,
      });
      debug('insertFile reponse', res);
      const fileId = _get(res, 'insert_file.returning[0].id', null);
      return fileId;
    } catch (err) {
      debug('insertFile error', err);
      throw err;
    }
  };
  const headers = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'OPTIONS, POST',
    'Access-Control-Max-Age': 2592000, // 30 days
    /** add other headers as per requirement */
  };

  server.options(uploadApiEndpoint, (req, res, next) => {
    try {
      res.writeHead(204, headers);
      return res.end();
    } catch (err) {
      return next(err);
    }
  });

  server.post(
    uploadApiEndpoint,
    // eslint-disable-next-line prefer-arrow-callback
    function authMiddleware(req, res, next) {
      if (!req.session.userId) {
        return res.send(403);
      }
      return next();
    },
    (req, res, next) => {
      try {
        // parse a file upload
        const form = new formidable.IncomingForm();
        form.uploadDir = uploadDir;
        form.keepExtensions = true;
        form.maxFileSize = FILE_SIZE_LIMIT * 1024 * 1024; // each file size limit 10 MB
        form.parse(req, async (err, fields, files) => {
          try {
            if (err) {
              debug(err);
              return next(err);
            }
            const file = files['files[]'];
            console.log('file', path.basename(file.path));
            const savedFileId = await insertFile({
              saved_name: path.basename(file.path),
              name: file && file.name,
              size: file && file.size,
              path: file && file.path,
              url: null,
              user_id: req.session.userId,
              post_id: null,
            });
            res.writeHead(200, headers);
            res.write(JSON.stringify({ fields, files, fileId: savedFileId }));
            return res.end();
          } catch (error) {
            return next(error);
          }
        });
      } catch (err) {
        next(err);
      }
    },
  );
};
