const { GraphQLClient } = require('graphql-request');

const nextConfig = require('../next.config');

const {
  serverRuntimeConfig: { HASURA_ADMIN_SECRET },
  env: { GRAPHQL_API_URL },
} = nextConfig;

const graphqlClient = new GraphQLClient(GRAPHQL_API_URL, {
  headers: {
    'X-Hasura-Admin-Secret': HASURA_ADMIN_SECRET,
  },
});

module.exports = {
  graphqlClient,
};
