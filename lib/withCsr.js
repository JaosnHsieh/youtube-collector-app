import dynamic from 'next/dynamic';

export default Component =>
  dynamic(
    () =>
      new Promise(resolve => {
        resolve(Component);
      }),
    { ssr: false },
  );
