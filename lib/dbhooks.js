import { useQuery, useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import _get from 'lodash/get';
import _set from 'lodash/set';
// import Debug from 'debug';

// const debug = Debug('YCA:lib:dbhooks.js');

export const USER = gql`
  query {
    user: me_user {
      id
      display_name
      files {
        # id
        path
        url
      }
    }
  }
`;
export const useCurrentUser = () => {
  const { data: dataFromUserQuery = {} } = useQuery(USER);
  const currentUser = _get(dataFromUserQuery, 'user[0]', null);
  return currentUser;
};

export const LIKE_FIELDS = `
  id
`;

export const INSERT_LIKE = gql`
  mutation($userId: Int!, $postId: Int!, $likeTypeId: Int!) {
    insert_like(objects: { user_id: $userId, post_id: $postId, like_type_id: $likeTypeId }) {
      returning {
        ${LIKE_FIELDS}
      }
    }
  }
`;

export const DELETE_LIKE = gql`
  mutation($userId: Int!, $postId: Int!, $likeTypeId: Int!) {
    delete_like(
      where: {
        _and: [
          { user_id: { _eq: $userId } }
          { post_id: { _eq: $postId }, like_type_id: { _eq: $likeTypeId } }
        ]
      }
    ) {
      returning {
        ${LIKE_FIELDS}
      }
    }
  }
`;

const POST_FIELDS = `
      id
      created_at
      title
      content
      files {
        id
        url
        saved_name
      }
      owner {
        id
        display_name
        files {
          id
          url
        }
      }
      myLikes:likes(where:$likesWhere){
        ${LIKE_FIELDS}
      }
`;

export const POSTS = {
  query: gql`
  query($limit: Int!, $offset: Int!, $where: post_bool_exp, $likesWhere: like_bool_exp) {
    posts: post(order_by: { created_at: desc }, offset: $offset, limit: $limit, where: $where) {
      ${POST_FIELDS}
    }
  }
`,
  getVariables: (userId = '') => ({
    limit: 10,
    offset: 0,
    ...(userId ? { likesWhere: { user_id: { _eq: userId } } } : {}),
  }),
};

export const POST_AGGREGATE = gql`
  query($where: post_bool_exp!) {
    post_aggregate(where: $where) {
      aggregate {
        count
      }
    }
  }
`;

export const POST = {
  query: gql`
  query($postId: Int!, $likesWhere: like_bool_exp) {
    post: post_by_pk(id:$postId) {
      ${POST_FIELDS}
    }
  }
`,
  getVariables: ({ postId, userId }) => ({
    postId,
    ...(userId ? { likesWhere: { user_id: { _eq: userId } } } : {}),
  }),
};

export const usePosts = ({ limit = 10, offset = 0 } = {}) => {
  const { data = {} } = useQuery(POSTS, { variables: { limit, offset } });
  const posts = _get(data, 'posts', []);
  return posts;
};

export const usePostsCount = ({ where = {} } = {}) => {
  const { data } = useQuery(POST_AGGREGATE, { variables: { where } });
  const count = _get(data, 'post_aggregate.aggregate.count', 0);
  return count;
};

export const useToggleLike = () => {
  const [deleteLike] = useMutation(DELETE_LIKE);
  const [insertLike] = useMutation(INSERT_LIKE);
  const toggleLike = ({ post = {}, user = {}, query, variables }) => {
    const isLiked = _get(post, 'myLikes', []).length > 0;
    const userId = _get(user, 'id');
    const postId = _get(post, 'id');
    if (isLiked) {
      deleteLike({
        variables: {
          userId,
          postId,
          likeTypeId: 1,
        },
        optimisticResponse: {
          __typename: 'Mutation',
          delete_like: {
            returning: [{ id: _get(post, 'myLikes[0].id'), __typename: 'like' }],
            __typename: '',
          },
        },
        update: (proxy, { data: deleteData = {} } = {}) => {
          if (_get(deleteData, 'delete_like.returning', []).length > 0) {
            if (query === POSTS.query) {
              const postsData = proxy.readQuery({
                query,
                variables,
              });
              const index = _get(postsData, 'posts', []).findIndex(
                p => _get(p, 'id') === _get(post, 'id'),
              );
              _set(postsData, `posts[${index}].myLikes`, []);
              proxy.writeQuery({
                query,
                variables,
                data: postsData,
              });
            }
          }
          if (query === POST.query) {
            const postData = proxy.readQuery({
              query,
              variables,
            });
            _set(postData, `post.myLikes`, []);
            proxy.writeQuery({
              query,
              variables,
              data: postData,
            });
          }
        },
      });
    } else {
      insertLike({
        variables: {
          userId,
          postId,
          likeTypeId: 1,
        },
        optimisticResponse: {
          __typename: 'Mutation',
          insert_like: {
            returning: [{ __typename: 'like', id: Math.random() }],
            __typename: '',
          },
        },
        update: (proxy, { data: insertData = {} } = {}) => {
          if (_get(insertData, 'insert_like.returning', []).length > 0) {
            if (query === POSTS.query) {
              const postsData = proxy.readQuery({
                query,
                variables,
              });
              const index = _get(postsData, 'posts', []).findIndex(
                p => _get(p, 'id') === _get(post, 'id'),
              );
              _set(postsData, `posts[${index}].myLikes`, [
                { ..._get(insertData, 'insert_like.returning[0]', {}) },
              ]);
              proxy.writeQuery({
                query,
                variables,
                data: postsData,
              });
            }
          }
          if (query === POST.query) {
            const postData = proxy.readQuery({
              query,
              variables,
            });
            _set(postData, `post.myLikes`, [
              { ..._get(insertData, 'insert_like.returning[0]', {}) },
            ]);
            proxy.writeQuery({
              query,
              variables,
              data: postData,
            });
          }
        },
      });
    }
  };

  return [toggleLike];
};

// TODO
export const INSERT_POST = gql`
  mutation($content: String!, $isPublished: Boolean!,$channelId:Int!, $likesWhere: like_bool_exp) {
    insert_post(objects: { content: $content, is_published: $isPublished,channel_id: $channelId }) {
      returning {
        ${POST_FIELDS}
      }
    }
  }
`;
export const UPDATE_FILE = gql`
  mutation($post_id: Int!, $ids: [Int!]!, $likesWhere: like_bool_exp) {
    update_file(where: { id: { _in: $ids } }, _set: { post_id: $post_id }) {
      affected_rows
    }
  }
`;

export const CHANNELS = gql`
  query($isDefault: Boolean!) {
    channel(where: { is_default: { _eq: $isDefault } }) {
      id
    }
  }
`;
