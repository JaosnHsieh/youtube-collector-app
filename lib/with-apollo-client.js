import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import { getDataFromTree } from 'react-apollo';
// import { getDataFromTree as getMarkupFromTree } from '@apollo/react-hooks';
// https://github.com/trojanowski/react-apollo-hooks/issues/52#issuecomment-456864176
// import { renderToString } from 'react-dom/server';
import Debug from 'debug';
import initApollo from './init-apollo';

const debug = Debug('YCA:with-apollo-client');
export default App => {
  class Apollo extends React.Component {
    static displayName = 'withApollo(App)';
    static async getInitialProps(ctx) {
      const {
        Component,
        router, // eslint-disable-next-line no-unused-vars
        ctx: { req },
      } = ctx;

      let appProps = {};
      if (App.getInitialProps) {
        appProps = await App.getInitialProps(ctx);
      }

      // Run all GraphQL queries in the component tree
      // and extract the resulting data
      // const apollo = initApollo();
      const apollo = process.browser
        ? initApollo()
        : initApollo(undefined, { sessionJwt: req && req.session && req.session.jwt });
      if (!process.browser) {
        try {
          // Run all GraphQL queries
          await getDataFromTree(
            <App {...appProps} Component={Component} router={router} apolloClient={apollo} />,
          );
          // // https://github.com/trojanowski/react-apollo-hooks/issues/52#issuecomment-456864176
          // await getMarkupFromTree({
          //   renderFunction: renderToString,
          //   tree: <App {...appProps} Component={Component} router={router} apolloClient={apollo} />,
          // });
        } catch (error) {
          // Prevent Apollo Client GraphQL errors from crashing SSR.
          // Handle them in components via the data.error prop:
          // https://www.apollographql.com/docs/react/api/react-apollo.html#graphql-query-data-error
          // https://github.com/apollographql/react-apollo/issues/615
          if (
            'graphQLErrors' in error &&
            'message' in error &&
            error.message === `GraphQL error: field "me_user" not found in type: 'query_root'`
          ) {
            // skip this error since it caused by the way I use hasura/graphql-engine.
            debug('user not logined hit any page on server side');
          } else {
            // eslint-disable-next-line no-console
            console.error('Error while running `getDataFromTree`', error);
          }
        }

        // getDataFromTree does not call componentWillUnmount
        // head side effect therefore need to be cleared manually
        Head.rewind();
      }

      // Extract query data from the Apollo store
      const apolloState = apollo.cache.extract();

      return {
        ...appProps,
        apolloState,
        ...(process.browser ? {} : { sessionJwt: req && req.session && req.session.jwt }),
      };
    }

    constructor(props) {
      super(props);

      this.apolloClient = initApollo(props.apolloState, { sessionJwt: props.sessionJwt });
    }

    render() {
      return <App {...this.props} apolloClient={this.apolloClient} />;
    }
  }
  Apollo.propTypes = {
    apolloState: PropTypes.any,
    sessionJwt: PropTypes.string,
  };
  return Apollo;
};
