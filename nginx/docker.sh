#! /bin/sh
docker stop my-nginx
docker rm -f my-nginx

docker run -d --restart always --name my-nginx \
-p 80:80 \
-v $(pwd)/nginx.conf:/etc/nginx/nginx.conf:ro -d \
nginx:1.17.0

#--network host \

docker logs -f my-nginx
echo "nginx docker is listening to 80"

