FROM node:8.15.0-alpine AS builder
WORKDIR /app
COPY . .
RUN yarn install 
RUN yarn run build

CMD node server.js