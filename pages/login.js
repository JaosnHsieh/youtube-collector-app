// import App from '../components/app';
import Button from '@material-ui/core/Button';

import Nav from '../components/nav';
// import Header from '../components/header';
// import ArticleList from '../components/articleLists';

export default () => (
  <>
    <Nav />
    {'Login'}
    <a href="/auth/google">Login Google</a>
    <form method="POST">
      <input name="email" />
      <input name="passwd" type="password" />
      <Button variant="contained" color="primary">
        submit
      </Button>
    </form>
  </>
);
