/* eslint-disable no-unused-vars */
import _get from 'lodash/get';
import _set from 'lodash/set';
import _has from 'lodash/has';
import { useMutation } from '@apollo/react-hooks';
import { Query, Mutation, withApollo, compose, graphql } from 'react-apollo';
import Router from 'next/router';
import Debug from 'debug';
import AppBar, { getAppBarPropsFromUser } from '../components/AppBar';
import ScrollPosts from '../components/ScrollPosts';
import CreatePostFormCard, { getCardPropsFromUser } from '../components/CreatePostFormCard';
import {
  useCurrentUser,
  USER,
  POSTS,
  POST,
  UPDATE_FILE,
  INSERT_POST,
  CHANNELS,
  INSERT_LIKE,
  DELETE_LIKE,
  useToggleLike,
} from '../lib/dbhooks';

import UnderDev from '../components/UnderDev';

const debug = Debug('YCA:pages:index.js');

// eslint-disable-next-line no-use-before-define
const CreatePostFormCardWithGraphql = getCreatePostFormCardWithGraphql();
const Home = () => {
  const user = useCurrentUser();
  const isLogined = Boolean(user);
  const [toggleLike] = useToggleLike();

  return (
    <div>
      <UnderDev />
      <AppBar
        {...getAppBarPropsFromUser(user)}
        onSearch={keyword => {
          Router.push(`/search?q=${keyword}`);
        }}
      />
      {/* TODO  */}
      {isLogined && <CreatePostFormCardWithGraphql {...getCardPropsFromUser(user)} />}

      <Query query={POSTS.query} variables={POSTS.getVariables(_get(user, 'id'))}>
        {({ data, fetchMore, loading }) => (
          <>
            <ScrollPosts
              posts={_get(data, 'posts', [])}
              isLoading={loading}
              onLoadMore={() =>
                fetchMore({
                  variables: {
                    offset: _get(data, 'posts', []).length,
                  },
                  updateQuery: (prev, { fetchMoreResult = [] }) => {
                    if (!fetchMoreResult) return prev;
                    return {
                      ...prev,
                      posts: [..._get(prev, 'posts', []), ...fetchMoreResult.posts],
                    };
                  },
                })
              }
              onClickFavoriteIcon={({ post }) => {
                toggleLike({
                  post,
                  user,
                  query: POSTS.query,
                  variables: POSTS.getVariables(_get(user, 'id')),
                });
              }}
            />
          </>
        )}
      </Query>
    </div>
  );
};

export default Home;

/**
 * getCreatePostFormCardWithGraphql: compose and graphql usage refers to
 * https://www.apollographql.com/docs/react/api/react-apollo/#configname
 * Define here simply for function hosting
 */
function getCreatePostFormCardWithGraphql() {
  return compose(
    withApollo,
    graphql(USER, { name: 'me' }),
    graphql(CHANNELS, { options: { variables: { isDefault: true } } }),
    graphql(UPDATE_FILE, { name: 'updateFile' }),
    graphql(INSERT_POST, { name: 'insertPost' }),
  )(props => {
    const user = _get(props, 'me.user[0]');
    const addOnePostToCacheById = async postId => {
      const { client = {} } = props;
      const { posts = [] } = client.cache.readQuery({
        query: POSTS.query,
        variables: POSTS.getVariables(_get(user, 'id')),
      });
      const { data: { post = {} } = {} } = await client.query({
        query: POST.query,
        variables: { postId },
      });
      client.cache.writeQuery({
        query: POSTS.query,
        /**
         * variables must be added
         * https://github.com/apollographql/apollo-client/issues/3909#issuecomment-516457707
         */
        variables: POSTS.getVariables(_get(user, 'id')),
        data: { posts: [post, ...posts] },
      });
    };

    return (
      <CreatePostFormCard
        {...props}
        onSubmit={async editingPost => {
          const defaultChannelId = _get(props, 'data.channel[0].id');
          const fileIds = _get(editingPost, 'fileIds', []);
          const insertedPostRes = await props.insertPost({
            variables: {
              content: editingPost.content,
              isPublished: true,
              channelId: defaultChannelId,
            },
          });
          const insertedPostId = _get(insertedPostRes, 'data.insert_post.returning[0].id');
          if (insertedPostId && fileIds.length > 0) {
            await props.updateFile({
              variables: {
                post_id: insertedPostId,
                ids: fileIds,
              },
            });
          }
          await addOnePostToCacheById(insertedPostId);
        }}
      />
    );
  });
}
