import { useState } from 'react';
import _memoize from 'lodash/memoize';
import _debounce from 'lodash/debounce';
import _pick from 'lodash/pick';
import _get from 'lodash/get';
import PropTypes from 'prop-types';
import { Query } from 'react-apollo';
import { withRouter } from 'next/router';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import AppBar, { getAppBarPropsFromUser } from '../components/AppBar';
import ScrollPosts from '../components/ScrollPosts';
import { useCurrentUser, POSTS, POST_AGGREGATE } from '../lib/dbhooks';
import useDebounce from '../components/hooks/useDebounce';

const getSearchVar = _memoize(keyword => ({
  limit: 5,
  offset: 0,
  where: { _or: [{ title: { _ilike: `%${keyword}%` } }, { content: { _ilike: `%${keyword}%` } }] },
}));

const Search = ({ router: { query: { q = '' } = {} } = {} }) => {
  const user = useCurrentUser();
  const [keyword, setKeyword] = useState(q);
  const debouncedKeyword = useDebounce(keyword, 300);

  return (
    <div>
      <Query query={POSTS.query} variables={getSearchVar(debouncedKeyword)}>
        {({ data, fetchMore, loading, refetch }) => {
          const debounceOnSearch = _debounce(k => {
            if (k && k.length > 0) {
              refetch(getSearchVar(k));
            }
            setKeyword(k);
          }, 300);

          return (
            <Query query={POST_AGGREGATE} variables={_pick(getSearchVar(keyword), 'where')}>
              {({ data: countData }) => {
                const count = _get(countData, 'post_aggregate.aggregate.count', 0);

                return (
                  <>
                    <AppBar
                      {...getAppBarPropsFromUser(user)}
                      defaultKeyword={q}
                      onSearch={k => {
                        debounceOnSearch(k);
                      }}
                      isInitFocusSearchInput
                    />
                    {keyword && (
                      <Card>
                        <CardContent>
                          <Typography variant="body2" color="textSecondary" component="p">
                            {count} results for <strong>{keyword} </strong>
                          </Typography>
                        </CardContent>
                      </Card>
                    )}
                    <ScrollPosts
                      hasMore={count !== 0}
                      posts={_get(data, 'posts', [])}
                      isLoading={loading}
                      onLoadMore={() =>
                        fetchMore({
                          variables: {
                            offset: _get(data, 'posts', []).length,
                          },
                          updateQuery: (prev, { fetchMoreResult = [] }) => {
                            if (!fetchMoreResult) return prev;
                            return {
                              ...prev,
                              posts: [..._get(prev, 'posts', []), ...fetchMoreResult.posts],
                            };
                          },
                        })
                      }
                    />
                  </>
                );
              }}
            </Query>
          );
        }}
      </Query>
    </div>
  );
};

Search.propTypes = {
  router: PropTypes.object,
};

export default withRouter(Search);
