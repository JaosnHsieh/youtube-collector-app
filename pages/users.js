// eslint-disable-next-line no-unused-vars
import React, { Fragment, useState } from 'react';
import { Query } from 'react-apollo';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import Nav from '../components/nav';

export const USER = gql`
  query {
    user {
      id
      name
      display_name
    }
  }
`;

export default function articleLists() {
  const { data: dataFromUserQuery = {} } = useQuery(USER);

  return (
    <Fragment>
      <pre>
        {`dataFromUserQuery`} {JSON.stringify(dataFromUserQuery)}
      </pre>
      <Query query={USER}>
        {({ data }) => (
          <div>
            from {`<Query />component`}
            {JSON.stringify(data)}
          </div>
        )}
      </Query>
      <Nav />
      {/* {data ? (
        <section>
          <pre>{JSON.stringify(data)}</pre>
        </section>
      ) : (
        <div>loading</div>
      )} */}
    </Fragment>
  );
}
