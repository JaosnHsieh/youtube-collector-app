import _get from 'lodash/get';
import PropTypes from 'prop-types';
import { Query } from 'react-apollo';
import { withRouter } from 'next/router';
// import Debug from 'debug';
import AppBar, { getAppBarPropsFromUser } from '../components/AppBar';
import PostCard from '../components/PostCard';
import { useCurrentUser, POST, useToggleLike } from '../lib/dbhooks';

// const debug = Debug('YCA:pages:post.js');

const Post = ({
  router: {
    query: { id: postId },
  },
}) => {
  const user = useCurrentUser();
  const [toggleLike] = useToggleLike();

  return (
    <div>
      <AppBar {...getAppBarPropsFromUser(user)} />
      <Query query={POST.query} variables={POST.getVariables({ postId, userId: _get(user, 'id') })}>
        {({ data }) => (
          <PostCard
            post={_get(data, 'post', {})}
            onClickFavoriteIcon={() => {
              toggleLike({
                post: _get(data, 'post', {}),
                user,
                query: POST.query,
                variables: POST.getVariables({ postId, userId: _get(user, 'id') }),
              });
            }}
          />
        )}
      </Query>
    </div>
  );
};

Post.propTypes = {
  router: PropTypes.object,
};
export default withRouter(Post);
